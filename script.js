const title = document.getElementById("task-title"),
  description = document.getElementById("task-description"),
  tableBody = document.getElementById("table-body"),
  searchInput = document.getElementById("search-input"),
  editTitle = document.getElementById("edit-title"),
  editDescription = document.getElementById("edit-description"),
  saveBtn = document.querySelector("#save-btn"),
  addBtn = document.querySelector("#add-btn"),
  searchBtn = document.getElementById("search-btn");

let taskArr = JSON.parse(localStorage.getItem("newTask")) || [];

function setArr() {
  localStorage.setItem("newTask", JSON.stringify(taskArr));
}

function taskRow(element, index) {
  return `<tr>
              <th scope="row">${index + 1}</th>
              <td>${element[0]}</td>
              <td>${element[1]}</td>
              <td>
                <button
                  class="btn btn-outline-secondary btn-sm"
                  type="button"
                  data-bs-toggle="modal"
                  data-bs-target="#edit-task" 
                  onclick = "editTask(${index})"
                >
                  Edit
                </button>
              </td>
              <td>
                <button
                  class="btn btn-outline-secondary btn-sm" 
                  onclick = "deleteTask(${index})" 
                  type="button"
                >
                  Delete
                </button>
              </td>
            </tr>`;
}
// populate the table
function populateTable() {
  let str = "";
  taskArr.forEach((element, index) => {
    str += taskRow(element, index);
  });
  tableBody.innerHTML = str;
}
// add tasks
function getAndPopulate() {
  titleValue = title.value;
  descriptionValue = description.value;

  taskArr.push([titleValue, descriptionValue]);
  setArr();
  populateTable();

  title.value = "";
  description.value = "";
}
// to delete tasks
function deleteTask(index) {
  taskArr.splice(index, 1);
  setArr();
  populateTable();
}
// to edit task
function editTask(index) {
  currentIndex = index;

  let element = taskArr[index];
  editTitle.value = element[0];
  editDescription.value = element[1];

  saveBtn.addEventListener("click", () => {
    editTitleValue = editTitle.value;
    editDescriptionValue = editDescription.value;

    taskArr[currentIndex][0] = editTitleValue;
    taskArr[currentIndex][1] = editDescriptionValue;

    setArr();
    populateTable();
  });
}
// search specific tasks
function searchTask() {
  let str = "";
  taskArr.forEach((element, index) => {
    if (element[0] == searchInput.value) {
      str += taskRow(element, index);
      tableBody.innerHTML = str;
    }
  });
}

addBtn.addEventListener("click", getAndPopulate);
searchBtn.addEventListener("click", searchTask);
populateTable();
